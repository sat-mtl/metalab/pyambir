
import numpy as np
import matplotlib.colors as colors


class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin: float = None,
                 vmax: float = None,
                 midpoint: float = None,
                 clip: bool = False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value: np.ndarray, clip: bool = None) -> np.ndarray:
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))
