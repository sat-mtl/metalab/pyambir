#!/usr/bin/python3
import argparse
import csv
import matplotlib.pyplot as plt
import math
import multiprocessing
import numpy as np
import operator
import time
import sys
import soundfile as sf

from datetime import datetime
from dtw import dtw
from dtw import warp
from dtw import rabinerJuangStepPattern
from math import factorial
from ir_config import IR_Config
from typing import List

parser = argparse.ArgumentParser(
    description="Interpolate a room impulse response at a given point in space from a set of measured room impulse responses. Multichannel impulse response recordings"
    " can also be interpolated. For example, an ambisonic IR at a particular point in space can be computed from an ambisonic set of measured IRs."
    ""
    " Using the dtw-python library, credits to: Giorgino. Computing and Visualizing Dynamic Time Warping Alignments in R: "
    " The dtw Package. J. Stat. Soft., 31 (2009)"
)
parser.add_argument(
    "x_coord",
    type=float,
    help="X-coordinate of the wanted point in space.",
)
parser.add_argument(
    "y_coord",
    type=float,
    help="Y-coordinate of the wanted point in space.",
)
parser.add_argument(
    "z_coord",
    type=float,
    help="Z-coordinate of the wanted point in space.",
)
parser.add_argument(
    "csv_filename",
    type=str,
    help="CSV file listing all measured IR's wav files and each of their coordinates in space separated by a coma. Format: FileName,x,y,z",
)
parser.add_argument(
    "-p",
    help="Plots the resulting interpolated IR on top of each of its references.",
    action="store_true",
)
parser.add_argument(
    "-m",
    help="Plots the matching achieved by the dtw method between the Impulse Reponses. Note that the execution of the program will stop after it.",
    action="store_true",
)

args = parser.parse_args()

#
#   Helper methods.
#
def get_area(s_1: float, s_2: float, s_3: float) -> float:
    """A method that returns the area of a triangle using Heron's formula, namely using the length of the sides of the triangle.

    Args:
        side_1 (float): length of side 1
        side_2 (float): length of side 2
        side_3 (float): length of isde 3

    Returns:
        float: area of the triangle
    """

    s = (s_1 + s_2 + s_3) / 2.0
    return math.sqrt(s * (s - s_1) * (s - s_2) * (s - s_3))


def assign_weights(a: IR_Config, b: IR_Config, c: IR_Config, p: IR_Config) -> None:
    """A method that computes the weights of each IRs using Barycentric coordinates.

    Args:
        a (IR_Config): an IR_Config used for interpolation
        b (IR_Config): an IR_Config used for interpolation
        c (IR_Config): an IR_Config used for interpolation
        p (IR_Config): the IR_Config we want to compute
    """

    # First we compute the lengths of all sides of the abc triangle.
    len_ab = a.get_distance_with(b)
    len_ac = a.get_distance_with(c)
    len_bc = b.get_distance_with(c)

    # Then we compute the lentghs of the segment between each peak and the wanted_point p.
    len_ap = a.get_distance_with(p)
    len_bp = b.get_distance_with(p)
    len_cp = c.get_distance_with(p)

    # Then we compute the weight of each IRs using the barycentric coordinates technique:
    #   weight_i = area_opposite_inner_triangle / area_outer_triangle
    area_abc = get_area(len_ab, len_ac, len_bc)

    # If the three points are on the same a line, then we take the distance in consideration, not the areas.
    if area_abc == 0:
        denominator = (1 / len_ap) + (1 / len_bp) + (1 / len_cp)
        a._weight = (1 / len_ap) / denominator
        b._weight = (1 / len_bp) / denominator
        c._weight = (1 / len_cp) / denominator
    # Otherwise, we compute the weight using the formula mentionned above.
    else:
        area_abp = get_area(len_ab, len_ap, len_bp)
        c._weight = area_abp / area_abc

        area_acp = get_area(len_ac, len_ap, len_cp)
        b._weight = area_acp / area_abc

        area_bcp = get_area(len_bc, len_bp, len_cp)
        a._weight = area_bcp / area_abc


def is_point_inside_triangle(
    a: IR_Config, b: IR_Config, c: IR_Config, p: IR_Config
) -> bool:
    """A method that checks if IR p is inside the triangle formed by IRs a, b, and c. 
    
    This method has been taken from "xApple" post on the following threads in stack overflow:
        https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle

    Explaination of the logic from author:
    "Walk clockwise or counterclockwise around the triangle and project the point onto the segment we are crossing
    by using the dot product. Finally, check that the vector created is on the same side for each of the triangle's 
    segments."

    Args:
        a (IR_Config): an Impulse Response, namely an IR_Config instance, of one of the three points in the triangle
        b (IR_Config): an Impulse Response, namely an IR_Config instance, of one of the three points in the triangle
        c (IR_Config): an Impulse Response, namely an IR_Config instance, of one of the three points in the triangle
        p (IR_Config): an Impulse Response, namely an IR_Config instance, of the point that we want to interpolate.

    Returns:
        [boolean]: returns whether or not the point is in the triangle.
    """

    x = p._params.x
    y = p._params.y
    ax = a._params.x
    ay = a._params.y
    bx = b._params.x
    by = b._params.y
    cx = c._params.x
    cy = c._params.y

    # Segment A to B
    side_1 = (x - bx) * (ay - by) - (ax - bx) * (y - by)
    # Segment B to C
    side_2 = (x - cx) * (by - cy) - (bx - cx) * (y - cy)
    # Segment C to A
    side_3 = (x - ax) * (cy - ay) - (cx - ax) * (y - ay)
    
    # All the signs must be positive or all negative
    return (side_1 < 0.0) == (side_2 < 0.0) == (side_3 < 0.0)


def is_point_in_zone(
    max_x: int, min_x: int, max_y: int, min_y: int, x: int, y: int
) -> bool:
    """A method that checks that the point's coordinates are within the interpolation area.

    Args:
        max_x (int): the maximum x-axis value
        min_x (int): the minimum x-axis value
        max_y (int): the maximum y-axis value
        min_y (int): the minimum y-axis value
        x (int): the wanted point x coordinate
        y (int): the wanted point y coordinate

    Returns:
        bool: returns whether or not the point is in the zone.
    """
    if x > max_x:
        return False
    if x < min_x:
        return False
    if y > max_y:
        return False
    if y < min_y:
        return False

    return True


def early_reflections_interpolation(
    wanted_point: IR_Config, reference: IR_Config, queries_list: List[IR_Config]
) -> None:
    """A method that interpolates the early reflections of the reference and of the queries in the queries list.
    The warp indices and the weights of each IR_Configs are taken into consideration during the interpolation process.

    Args:
        wanted_point (IR_Config): IR_Config instance of the point that we wish to compute.
        reference (IR_Config): IR_Config instance of a measured impulse response that we want to interpolate.
        queries_list (List[IR_Config]): list of IR_Config instance of measured impulse responses that we want to interpolate.
    """

    # Initializing a list to store every channels (as list) for the early reflections of the wanted point. 
    wanted_point._er_as_array = []
    
    # For every channel in the IRs
    for channel in range(reference._number_of_channels):

        # Initializing an array of tuples (interpolated_position, interpolated_amplitude)
        interpolation_data_array = []

        # For each frame of the reference IR
        # - we compute the interpolated amplitude of the frame  for the reference and the query
        # - we compute the interpolated position of the frame
        # - we add the interpolated positon and amplitude of the frame on the interpolation_data_array as a tuple
        for position in range(reference._er_as_array[channel].size - 1):

            # Getting each amplitude from the different IRs at the particular momentum using their warp table indices.
            reference._amplitude_at_position = reference._er_as_array[channel][position]

            for query in queries_list:
                index_in_query = int(query._warp_table[channel][position])
                query._amplitude_at_position = query._er_as_array[channel][index_in_query]

            # Adding them together taking the weight of each IR into account.
            amplitude_interpolated = 0.0

            amplitude_interpolated += (
                reference._weight * reference._amplitude_at_position
            )

            for query in queries_list:
                amplitude_interpolated += query._weight * query._amplitude_at_position

            # Getting each frame position from the different IRs at the particular momentum
            # Adding them together taking the _weight of each IR into account
            index_interpolated = 0.0

            index_interpolated += reference._weight * position

            for query in queries_list:
                index_interpolated += (
                    query._weight * query._warp_table[channel][position]
                )

            interpolation_data_array.append(
                (index_interpolated, amplitude_interpolated)
            )

        # Initializing a list for the interpolated frames of the current channel of the wanted point.
        current_channel_list = []       

        # For every position in the wanted array
        #   - we gather the two closest position indices from the interpolation_data_array to the current position
        #   - we compute the weights of each position index and retrieve the amplitudes at these positions
        #   - we add those interpolated amplitudes at the current position.
        raw_index = 0
        for position in range(len(interpolation_data_array)):

            # We find the next bigger raw index in the interpolation_data_array
            while float(position) > interpolation_data_array[raw_index][0]:
                raw_index += 1
                if raw_index >= len(interpolation_data_array):
                    break

            if raw_index >= len(interpolation_data_array):
                break

            # If the position corresponds exactly to the index at i, then we add this amplitude to the current_channel_list
            if float(position) == interpolation_data_array[raw_index][0]:
                current_channel_list.append(interpolation_data_array[raw_index][1])
            # Otherwise, we compute the weights of the two values surrounding the wanted position in
            #  the interpolation_data_array and add them together to the current_channel_list
            else:
                amp_a = interpolation_data_array[raw_index - 1][1]
                amp_b = interpolation_data_array[raw_index][1]

                index_a = interpolation_data_array[raw_index - 1][0]
                index_b = interpolation_data_array[raw_index][0]
                weight = (float(position) - index_a) / (index_b - index_a)

                current_channel_list.append(weight * amp_a + (1 - weight) * amp_b)

        # Once all positions have been interpoaleted, we add the list corresponding to 
        # the current channel to the list of list of the wanted point
        wanted_point._er_as_array.append(current_channel_list)

    print("[Status]: Done computing interpolation for early reflections.")


def tails_interpolation(
    wanted_point: IR_Config, reference: IR_Config, queries_list: List[IR_Config]
) -> None:
    """A method that interpolates the tails of the reference and of the queries in the queries list.
    The weights of each IRs are taken into consideration during the interpolation process.

    Args:
        wanted_point (IR_Config): IR_Config instance of the point that we wish to compute.
        reference (IR_Config): IR_Config instance of a measured Impusle response that we want to interpolate.
        queries_list (List[IR_Config]): list of IR_Config instance of measured impulse responses that we want to interpolate.
    """

    # Here the idea is to take the amplitudes at each position and to merge them according to their weights.
    # Getting the biggest tail_array size to intialize the wanted_point tail array.
    tail_sizes = []
    tail_sizes.append(reference._tail_as_array[0].size)
    for query in queries_list:
        tail_sizes.append(query._tail_as_array[0].size)

    biggest_size = max(tail_sizes)
    smallest_size = min(tail_sizes)
    wanted_point._tail_as_array = np.zeros(
        (reference._number_of_channels, biggest_size)
    )

    # For every channel in the IRs
    for channel in range(reference._number_of_channels):

        # For each frame of the IR to interpolate,
        # - we compute the interpolated amplitude of the frame 
        # - we add the frame of the computed amplitude at the current positon
        for position in range(smallest_size - 1):

            # Getting all amplitudes from the different IRs at the particular position.
            reference._amplitude_at_position = reference._tail_as_array[channel][
                position
            ]

            for query in queries_list:
                query._amplitude_at_position = query._tail_as_array[channel][position]

            # Adding them together taking the weight of each IR into accounts.
            amplitude_interpolated = 0.0

            amplitude_interpolated += (
                reference._weight * reference._amplitude_at_position
            )

            for query in queries_list:
                amplitude_interpolated += query._weight * query._amplitude_at_position

            wanted_point._tail_as_array[channel][position] = amplitude_interpolated

    print("[Status]: Done computing interpolation for the tails.")


def savitzky_golay(
    y: np.array, window_size: int, order: int, deriv=0, rate=1
) -> np.array:
    """A method to apply a Savitzky Golay filter, to smooth the interpolated array so there is no unwanted high freqeuncy noise.

    Credits: This method has been taken from: https://scipy-cookbook.readthedocs.io/items/SavitzkyGolay.html

    Quote from the author: " The main idea behind this approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at the point.".

    Args:
        y (np.array): the array on which we want to apply the filter
        window_size (int): size of the window
        order (int): order of the polynomial
        deriv (int, optional): Defaults to 0. No other value used.
        rate (int, optional): Defaults to 1. No other value used.

    Returns:
        np.array: the array that has been smoothed by the filter.
    """
    try:
        window_size = np.abs(int(window_size))
        order = np.abs(int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")

    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")

    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")

    order_range = range(order + 1)
    half_window = (window_size - 1) // 2

    # precompute coefficients
    b = np.mat(
        [[k ** i for i in order_range] for k in range(-half_window, half_window + 1)]
    )
    m = np.linalg.pinv(b).A[deriv] * rate ** deriv * factorial(deriv)

    # pad the signal at the extremes with values taken from the signal itself
    firstvals = y[0] - np.abs(y[1 : half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1 : -1][::-1] - y[-1])

    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode="valid")


def generate_WAV(a_computed_ir: IR_Config) -> None:
    """A method that generates a wave file (.wav) from the array representing the interpolated Impulse Response.
    The name of the generated file is "result_HH:MM:SS_(x_coord, y_coord, z_coord).wav" and it is created where this script is being executed.

    Args:
        a_computed_ir (IR_Config): the IR_Config instance for which we wish to generate a wave file.
    """

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    coords = (
        str(a_computed_ir._params.x)
        + " ,"
        + str(a_computed_ir._params.y)
        + " ,"
        + str(a_computed_ir._params.z)
    )
    output_file = f"resultIR_{current_time}_({coords}).wav"

    # Writing the array into a wav file.
    sf.write(output_file, a_computed_ir._merged_array.T, 48000)


def compute_dtw(reference_array: np.ndarray, query_array: np.ndarray, query_warp_table: np.ndarray, channel: int, print_matching: bool) -> None:
    """A method to compute the dynamic time warping between the reference and the query. It allows
    to compute the warp indices table for the given query in order to align all samples.

    Args:
        reference (IR_Config): The IR_Config object that serves as a reference for the dtw
        query (IR_Config): The IR_Config object that serves as quuery for the dtw
        channel (int): the channel number of the files that we want to compute.
    """

    # NOTE: I currently use a "Rabiner-Juang step pattern" of type "V" (5) and with a slope weight of type "a"
    # (see Table 4.5 and Fig 4.43 in "Fundamental of Speech Recognition - (Lawrence Rabiner - Biing Hwang Juang) - 1993" paper).
    # There are several step patterns that may be more adequate to certain types of Impulse Responses (duration, clear reflections, etc.)
    # See dtw-python doc for more information on step patterns:
    # https://dynamictimewarping.github.io/py-api/html/api/dtw.StepPattern.html#dtw.StepPattern

    alignment = None

    # If conditions are met, this will print the matching done by the dtw. 
    # It will plot a graph, BUT will stop the execution.
    if print_matching and channel == 0:
        alignment = dtw(
            query_array[channel],
            reference_array[channel],
            keep_internals=True,
            step_pattern=rabinerJuangStepPattern(5,"a")
            )\
        .plot(type="twoway",offset=-2)
        return
    # Otherwise, we compute the dynamic time warping normally
    else:
        alignment = dtw(
            query_array[channel],
            reference_array[channel],
            keep_internals=True,
            step_pattern=rabinerJuangStepPattern(5, "a")
        )

    # Computing the warp indices table.
    wq = warp(alignment, index_reference=False)

    query_warp_table[channel] = wq


#
#   Main method.
#
def interpolate_ir(
    x_coord: float, y_coord: float, z_coord: float, file_measured_irs: str, plot: bool, print_dtw_matching: bool
) -> None:
    """This function interpolates several measured Impulse Responses (IRs) in space at a specified position in space.

    Args:
        x_coord (int): X coordinate of the wanted point in space.
        y_coord (int): Y coordinate of the wanted point in space.
        z_coord (int): Z coordinate of the wanted point in space.
        file_measured_irs (csv-file): csv file that specifies the Impulse Responses file paths and their coordinates.
        plot (boolean): set to true if -p was specified when launching the code, if true, plots the results.
    """

    # STEP 1, create IR_Config instances...
    # ...for the wanted point (wanted_point)
    wanted_point = IR_Config(None, (x_coord, y_coord, z_coord))

    # ...for all measured IRs from the "some_Measured_IRs" file, append them to measured_irs.
    measured_irs: List[IR_Config] = []
    max_x: float = -sys.maxsize - 1
    min_x: float = sys.maxsize
    max_y: float = -sys.maxsize - 1
    min_y: float = sys.maxsize

    with open(file_measured_irs) as csv_file:

        readCSV = csv.reader(csv_file, delimiter=",")
        first_line_flag = True

        for row in readCSV:

            if first_line_flag:
                first_line_flag = False
            else:
                measured_irs.append(
                    IR_Config(row[0], (float(row[1]), float(row[2]), float(row[3])))
                )

                # Computing the maximum and minimum value for the x and y axis.
                if float(row[1]) > max_x:
                    max_x = float(row[1])
                if float(row[1]) < min_x:
                    min_x = float(row[1])
                if float(row[2]) > max_y:
                    max_y = float(row[2])
                if float(row[2]) < min_y:
                    min_y = float(row[2])

    print("[Status]: Done with the initialisation of instances.")

    # STEP 2: select IR that we want to interpolate.
    # The idea is to select the 3 closest IRs to the wanted_point
    # and to compute their _weights using barycentric coordinates.

    # Computing the distances from each measured points to the wanted point
    for ir in measured_irs:
        dist = wanted_point.get_distance_with(ir)
        ir._distance_to_point = dist

    # Sorting the list to select the 3 closest IRs
    sorted_irs = sorted(measured_irs, key=operator.attrgetter("_distance_to_point"))
    reference: IR_Config = sorted_irs[0]
    query1: IR_Config = sorted_irs[1]
    query2: IR_Config = sorted_irs[2]
    counter = 2
    index_error_flag = False

    # Check that the point coordinates are not out of the interpolation range
    if not is_point_in_zone(
        max_x, min_x, max_y, min_y, wanted_point._params.x, wanted_point._params.y
    ):
        print(
            "[Error 1]: Coordinates of the wanted point is not inside the interpolation zone, please use coordinates between at least 3 points."
        )
        index_error_flag = True

    # Making sure that the point is inside the triangle formed by the selected IRs.
    # If not, we replace query2 by next one in sorted_irs list.
    # If all irs have been tried and the point is still not inside a triangle, then the point in not computable.
    while index_error_flag == False:
        if is_point_inside_triangle(reference, query1, query2, wanted_point):
            break
        counter += 1
        try:
            query2 = sorted_irs[counter]
        except IndexError:
            print(
                "[Error 2]: Coordinates of the wanted point is not inside the interpolation zone, please use coordinates between at least 3 points."
            )
            index_error_flag = True
            break

    if index_error_flag:
        return

    assign_weights(reference, query1, query2, wanted_point)

    # Set queries.
    queries_list: List[IR_Config] = []
    queries_list.append(query1)
    queries_list.append(query2)

    # Used for navigation informations. 
    #  
    # print(f"({x_coord}, {y_coord})")
    # print(f"   - {reference.a_file} ({reference._params.x}, {reference._params.y})")
    # for query in queries_list:
    #     print(f"   - {query.a_file} ({query._params.x}, {query._params.y})")

    # Printing the weight of each selected IRs for the interpolation.
    print(f"[Info] Measured IRs used for the interpolation at point ({x_coord}, {y_coord}, {z_coord}): ")
    print(f'    - "{reference.a_file}" with weight : {reference._weight}')
    for query in queries_list:
        print(f'    - "{query.a_file}" with weight : {query._weight}')


    # STEP 3, Aligning samples using Dynamic Time Warping
    # Computing DTW between the reference and each queries to get their warp tables,
    # i.e., tables that match a frame in the queries to a corresponding frame in the reference.
    # The DTW is computed for each channel of the IRs using multiprocessing to spped up the process.
    query_number = 1
    for query in queries_list:
        print(
            f"[Status]: DTW for query {query_number} / {query._number_of_channels} channel(s) to compute...",
            end=" ",
            flush=True,
        )

        # If we have mono files or we wish to print the dtw matching, then we don't use multiprocessing.
        if reference._number_of_channels == 1 or print_dtw_matching:
            compute_dtw(reference._er_as_array, query._er_as_array, query._warp_table, 0, print_dtw_matching)

        # Otherwise, we use multiprocessing to speed up the process.
        else:
            for channel in range(reference._number_of_channels):
                compute_dtw(reference._er_as_array, query._er_as_array, query._warp_table, channel, print_dtw_matching)

            # # Broken implementation of multiprocessing.
            #
            # # Creating the processes.
            # processes = []
            #
            # for channel in range(reference._number_of_channels):
            #     p = multiprocessing.Process(
            #         target = compute_dtw, args=[reference._er_as_array, query._er_as_array, query._warp_table, channel, print_dtw_matching]
            #     )
            #     p.start()
            #     processes.append(p)
            # # Joining them.
            # for process in processes:
            #     process.join()


        # If we have printed the dtw matching, we are exiting the program.
        if print_dtw_matching:
            print("\n[Status]: Exiting.")
            return

        query_number += 1
        print("Done.")

    # STEP 4, Interpolation of early reflections of the Impulse Responses.
    early_reflections_interpolation(wanted_point, reference, queries_list)

    # For each channel of the interpolated array, we apply a filter that smoothes the curves (and remove noise in the high frequencies)
    for channel in range(reference._number_of_channels):
        wanted_point._er_as_array[channel] = savitzky_golay(
            wanted_point._er_as_array[channel], 11, 5
        )

    # STEP 5, Interpolation of the tails of the Impulse Responses.
    tails_interpolation(wanted_point, reference, queries_list)

    # STEP 6, Merging the early reflections array and the tail array back together.
    wanted_point.merge_arrays(reference._number_of_channels)

    # STEP 7, Generating a wave file.
    generate_WAV(wanted_point)

    # (Optional) Ploting the results.
    if plot:
        reference.merge_arrays(reference._number_of_channels)
        for query in queries_list:
            query.merge_arrays(query._number_of_channels)

        figs, arr = plt.subplots(3)
        figs.suptitle(
            f"First channels of the interpolated IR (orange) on top of each of the 3 measured IRs used for interpolation."
        )
        arr[0].plot(reference._merged_array[0])
        arr[0].plot(wanted_point._merged_array[0])

        for i in range(2):
            arr[i + 1].plot(queries_list[i]._merged_array[0])
            arr[i + 1].plot(wanted_point._merged_array[0])

        plt.show()


if __name__ == "__main__":
    interpolate_ir(args.x_coord, args.y_coord, args.z_coord, args.csv_filename, args.p, args.m)
    print()

    # Used to debug. 
    # interpolate_ir(1.6, 2.2, 1.0, "use_cases/centech/centech_IR.csv", True, False)
    # interpolate_ir(3.0, 12.0, 1.0, "use_cases/maison_symph_S04/maison_symph_S04.csv", True, False)
    # interpolate_ir(7.0, 1.5, 1.0, "use_cases/maison_symph_ambi_iter2/maison_symph_ambi_iter2.csv", True, False)
